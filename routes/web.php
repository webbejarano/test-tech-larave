<?php

use App\Http\Controllers\LogsWebSiteController;
use App\Http\Controllers\WebSiteURLController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get("/", function () {
    return redirect("/websiteurl");
});

Route::get("logs/{websiteId}", [LogsWebSiteController::class, 'index'])
    ->middleware('auth')->name('websitelogs');

Route::resource('websiteurl', WebSiteURLController::class)
    ->middleware('auth');

