<?php

namespace App\ValueObjects;

use Psr\Log\InvalidArgumentException;

class URLObjects
{
    public function __construct(private string $url)
    {

        if (!filter_var($url, FILTER_VALIDATE_URL)) {
            throw new InvalidArgumentException('Invalid URL');
        }

    }

    public function getURL(): string
    {
        return $this->url;
    }
}
