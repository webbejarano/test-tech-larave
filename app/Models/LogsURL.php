<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class LogsURL extends Model
{
    use HasFactory;

    protected $table = 'logs_urls';

    protected $fillable = [
        "http_code",
        "response",
        "web_site_url_id"
    ];

    public function getWebSiteURL(): belongsTo {
        return $this->belongsTo(WebSiteURL::class,"web_site_url_id", "id");
    }
}
