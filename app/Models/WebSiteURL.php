<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class WebSiteURL extends Model
{
    use HasFactory;
    protected $table = "web_site_urls";

    protected $fillable = [
        "url",
        "user_id",
    ];

    public function getLogs(): HasMany {
        return $this->hasMany(LogsURL::class, 'web_site_url_id', 'id');
    }

    public function getUser(): HasMany {
        return $this->hasMany(User::class, 'user_id', 'id');
    }
}
