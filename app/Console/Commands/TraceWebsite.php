<?php

namespace App\Console\Commands;

use App\Models\LogsURL;
use App\Models\WebSiteURL;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class TraceWebsite extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:trace-website';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Rastreamento de status de websites';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $urls =  WebSiteURL::all();

        $urls->each(function ($url) {
            $response = Http::get($url->url);
            LogsURL::create([
                "http_code" => $response->status(),
                "response" => $response->body(),
                "web_site_url_id" => $url->id
            ]);
        });

        return 0;
    }
}
