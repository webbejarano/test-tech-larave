<?php

namespace App\Http\Controllers;

use App\Models\WebSiteURL;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class WebSiteURLController extends Controller
{
    public function __construct(private WebSiteURL $webSiteURLModel)
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return Renderable
     */
    public function index(): Renderable
    {
        $paginate = $this->webSiteURLModel
            ->where('user_id', auth()->user()->id)
            ->paginate(10);

        return view('website-url-list', [
            'websiteurls' => $paginate,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Renderable
     */
    public function create(): Renderable
    {
        return view('website-url');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $request->except('_token');
        $fields = $request->all();
        $fields['user_id'] = auth()->user()->id;

        $this->webSiteURLModel->fill($fields);

        $this->webSiteURLModel->save();

        return redirect('/websiteurl/');

    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
