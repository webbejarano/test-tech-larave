<?php

namespace App\Http\Controllers;

use App\Models\LogsURL;
use Illuminate\Contracts\Support\Renderable;

class LogsWebSiteController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() { }

    /**
     * Display a listing of the resource.
     *
     * @return Renderable
     */
    public function index(string $websiteId):Renderable
    {
        $webSiteLogs = LogsURL::where("web_site_url_id", $websiteId)->paginate();

        return view('logs-urls', ['webSites' => $webSiteLogs]);
    }
}
