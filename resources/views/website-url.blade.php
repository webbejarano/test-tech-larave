@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <h2>URL</h2>
            <form method="post" action="/websiteurl/" class="row g-3">
                @csrf

                <div class="col-md-6">
                    <label for="url-site" class="form-label">URL do Site</label>
                    <input type="url" class="form-control" id="url-site" name="url">
                </div>

                <div class="col-12">
                    <button type="submit" class="btn btn-primary">Gravar</button>
                </div>
            </form>
        </div>
    </div>
@endsection
