@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="col-md-8">
                <div class="row">
                    <div class="col-10"><h1>Listado logs</h1></div>
                </div>
            </div>

            <table class="table table-hover">
                <thead class="table-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">URL</th>
                    <th scope="col">HTTP CODE</th>
                    <th scope="col">DATE</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($webSites as $log)
                    <tr>
                        <th scope="row">{{ $log->id }}</th>
                        <td>{{ $log->getWebSiteURL->url }}</td>
                        <td>{{ $log->http_code }}</td>
                        <td>{{ $log->created_at }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>

            {{ $webSites->links() }}

        </div>
    </div>
@endsection
