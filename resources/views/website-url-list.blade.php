@extends('layouts.app')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="row">
                <div class="col-10"><h1>Listado</h1></div>
                <div class="col-2">
                    <a href="/websiteurl/create" class="btn btn-success">
                        Novo
                    </a>
                </div>
            </div>


            <table class="table table-hover">
                <thead class="table-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">URL</th>
                    <th scope="col">Acciones</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($websiteurls as $website)
                    <tr>
                        <td>
                            {{ $website->id }}
                        </td>
                        <td>
                            <a href="{{ $website->url }}">{{ $website->url }}</a>
                        </td>
                        <td>
                            <a class="btn btn-secondary" href="{{ route('websitelogs', $website->id) }}">Logs</a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            {{ $websiteurls->links() }}
        </div>
    </div>

@endsection
