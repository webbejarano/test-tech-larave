<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('logs_urls', function (Blueprint $table) {
            $table->id();
            $table->string("http_code", 100);
            $table->text("response");
            $table->unsignedInteger('web_site_url_id');

            $table->foreign('web_site_url_id')->references('id')->on('web_site_urls')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('logs_urls', function (Blueprint $table) {
            $table->dropForeign('web_site_url_id');
            $table->dropColumn('web_site_url_id');
        });

        Schema::dropIfExists('logs_urls');
    }
};
